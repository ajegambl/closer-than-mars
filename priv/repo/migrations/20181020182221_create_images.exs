defmodule Mothership.Repo.Migrations.CreateImages do
  use Ecto.Migration

  def change do
    create table("images") do
      add(:name, :string)
      add(:rover, :string)
      add(:url, :string)
      add(:camera_used, :string)
      add(:description, :string)
      add(:confidence, :float)
      add(:date, :string)
      add(:tags, :string)

      timestamps
    end
  end
end
