use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :mothership, MothershipWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :mothership, Mothership.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "mothership_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
