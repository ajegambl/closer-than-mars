# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :mothership,
  ecto_repos: [Mothership.Repo]

# Configures the endpoint
config :mothership, MothershipWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Md/ITThT6K2w0vyjaPfNQv3vJ1kr8Vf2xDJnR86tcDsmRGpZczGC0jZSECY+ytgc",
  render_errors: [view: MothershipWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Mothership.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
