defmodule MothershipWeb.ApiController do
    use MothershipWeb, :controller
    alias Mothership.Query

    def upload(conn, params) do
        IO.inspect params
        case Mothership.Store.data(params) do
            {:ok, _resp} -> 
                resp = %{
                    "body" => "Image resource has been stored."
                } 
    
                conn
                |> put_status(:created)
                |> json(resp)

            {:error, changeset} -> 
                resp = %{
                    "body" => "Image resource could not be uploaded, see errors.",
                    "errors" => inspect(changeset.errors)
                } 
    
                conn
                |> put_status(:bad_request)
                |> json(resp)
        end
    end

    def tag_search(conn, %{"mode" => "strict", "tags" => tags}) do
        resp = %{
            "body" => Query.tags_strict(tags)
        }

        conn
        |> put_status(:ok)
        |> json(resp)
    end

    def tag_search(conn, %{"mode" => "loose", "tags" => tags}) do
        resp = %{
            "body" => Query.tags_loose(tags)
        }

        conn
        |> put_status(:ok)
        |> json(resp)
    end

    def tag_search(conn, _params) do
        resp = %{
            "body" => [],
            "errors" => "Please provide a mode{\"strict\" or \"loose\"} and a list of tags{string}"
        }

        conn
        |> put_status(:bad_request)
        |> json(resp)
    end
end
