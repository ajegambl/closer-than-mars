defmodule MothershipWeb.PageController do
  use MothershipWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
