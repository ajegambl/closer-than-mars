defmodule MothershipWeb.Router do
  use MothershipWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", MothershipWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", MothershipWeb do
    pipe_through :api

    post "/upload/image", ApiController, :upload
    post "/search/images", ApiController, :tag_search
  end
end
