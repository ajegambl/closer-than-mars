defmodule Mothership.Store do
    alias Mothership.Image

    def data(incoming) do
        data = Poison.decode!(incoming["data"])
        
        refined = %{
            "name" => Kernel.to_string(data["id"]),
            "rover" => data["rover_name"],
            "url" => data["img_src"],
            "camera_used" => data["camera_name"],
            "description" => data["description"]["text"],
            "confidence" => data["description"]["confidence"],
            "date" => data["earth_date"],
            "tags" => data["description"]["tags"]
        }

        new_data = 
            case refined["tags"] do
                nil ->
                    data
                tags ->
                    tag_string = Enum.join(tags, ",")
                    Map.merge(refined, %{"tags" => tag_string})
            end

        changeset = Image.changeset(%Image{}, new_data)

        Mothership.Repo.insert(changeset)
    end
end
