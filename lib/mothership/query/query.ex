defmodule Mothership.Query do
    import Ecto.Query
    alias Mothership.Image
    alias Mothership.Repo

    @required_fields [:name, :rover, :url, :camera_used, :description, :confidence, :date, :tags]

    def all do
        # Create a query
        query = from i in Image

        # Send the query to the repository
        Mothership.Repo.all(query)
    end

    def tags_loose([]), do: []
    def tags_loose([head | tail]) do
        initial = "SELECT * FROM images WHERE images.tags like \'%#{head}%\'"
        query = 
            Enum.reduce(tail, initial, fn(tag, acc) -> 
                acc <> "OR images.tags like \'%#{tag}%\'"
            end)

        res = Ecto.Adapters.SQL.query!(Repo, query, []) # a

        cols = Enum.map res.columns, &(String.to_atom(&1)) # b

        roles = Enum.map res.rows, fn(row) ->
          struct(Image, Enum.zip(cols, row)) # c
        end

        process_resp(roles)
    end

    def tags_strict([]), do: []
    def tags_strict([head | tail]) do
        initial = "SELECT * FROM images WHERE images.tags like \'%#{head}%\'"
        query = 
            Enum.reduce(tail, initial, fn(tag, acc) -> 
                acc <> "AND images.tags like \'%#{tag}%\'"
            end)

        res = Ecto.Adapters.SQL.query!(Repo, query, []) # a

        cols = Enum.map res.columns, &(String.to_atom(&1)) # b

        roles = Enum.map res.rows, fn(row) ->
          struct(Image, Enum.zip(cols, row)) # c
        end

        process_resp(roles)
    end

    def refine_by_tag(query, tag) do
        from q in query, where: like(q.tags, ^tag)
    end

    def process_resp(list) do
        if length(list) != 0 do
            list
            |> Enum.map(fn(item) -> take_fields(item) end)
        end
    end

    def take_fields(item) do
        Map.take(item, @required_fields)
    end
end
