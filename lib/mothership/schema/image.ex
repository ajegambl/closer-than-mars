defmodule Mothership.Image do
    use Ecto.Schema
    import Ecto.Changeset

    schema "images" do
        field(:name, :string)
        field(:rover, :string)
        field(:url, :string)
        field(:camera_used, :string)
        field(:description, :string)
        field(:confidence, :float)
        field(:date, :string)
        field(:tags, :string)
  
      timestamps()
    end

    @required_fields [:url, :name, :rover, :camera_used, :description, :confidence, :date, :tags]

    def changeset(image, params \\ %{}) do
        image
        |> cast(params, @required_fields)
    end
end
